package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="viewLead_firstName_sp") WebElement eleVerifyLeadName;
	@FindBy(how=How.LINK_TEXT,using="Edit") WebElement eleEdit;
	@FindBy(how=How.ID,using="viewLead_companyName_sp") WebElement eleVerifyCmpName;
	@FindBy(how=How.ID,using="viewLead_companyName_sp") WebElement eleVerifyLeadId;
	
	public ViewLeadPage verifyLead(String data) {
		verifyExactText(eleVerifyLeadName, data);
		return this;
	}

	public UpdateLeadPage clickEdit() {
		click(eleEdit);
		return new UpdateLeadPage();
	}
	
	public ViewLeadPage verifyCompanyName(String data) {
		verifyPartialText(eleVerifyCmpName, data);
		return this;
	}
	
	public ViewLeadPage verifyLeadId() {
		verifyExactText(eleVerifyLeadId, "10443");
		return this;
	}
	

	
	
}
