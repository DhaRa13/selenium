package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeadPage extends Annotations {
	
	public MergeLeadPage() {
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(how=How.XPATH,using="(//img[@alt='Lookup'])[1]") WebElement eleFrom;
	@FindBy(how=How.XPATH,using="(//img[@alt='Lookup'])[2]") WebElement eleTo;
	@FindBy(how=How.CLASS_NAME,using="buttonDangerous") WebElement eleMerge;
	
	public FindLeadPage clickFrom() {
		clickWithNoSnap(eleFrom);
		switchToWindow(1);
		return new FindLeadPage();
	}
	
	public FindLeadPage clickTo() {
		switchToWindow(0);
		clickWithNoSnap(eleTo);
		switchToWindow(1);
		return new FindLeadPage();
	}
	
	public void clickMerge() {
		switchToWindow(0);
		clickWithNoSnap(eleMerge);
		acceptAlert();
		//return new ViewLeadPage();
	}
	
	

}
