package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCmpName;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleCreateLead;
	
	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(eleCmpName, data);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}
	
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}
	
	public ViewLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new ViewLeadPage();
	}
	
	
	

}
