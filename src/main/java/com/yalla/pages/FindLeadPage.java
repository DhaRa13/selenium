package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadPage extends Annotations {
	
	public FindLeadPage() {
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using="(//input[@name='lastName'])[3]") WebElement eleLastName;
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindLead;
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[6]") WebElement eleFstRstName;
	@FindBy(how=How.NAME,using="firstName") WebElement eleFstName;
	@FindBy(how=How.NAME,using="companyName") WebElement eleCmpName;
	@FindBy(how=How.CLASS_NAME,using="linktext") WebElement eleFstRstLead;
	
		
	public FindLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}
	
	public FindLeadPage enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}
	
	public FindLeadPage clickFindLead() {
		click(eleFindLead);
		return this;
	}
	
	public ViewLeadPage clickFirstResultingName() {
		click(eleFstRstName);
		return new ViewLeadPage();
		
	}
	
	public FindLeadPage typeFirstName(String data) {
		switchToWindow(1);
		clearAndType(eleFstName, data);
		return this;
	}
	
	public FindLeadPage typeCompanyName(String data) {
		//switchToWindow(1);
		clearAndType(eleCmpName, data);
		return this;
	}
	

	public MergeLeadPage clickFirstResultingLead() {
		clickWithNoSnap(eleFstRstLead);
		//switchToWindow(0);
		return new MergeLeadPage();
		
	}
	
	
	
}
