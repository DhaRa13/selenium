package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class UpdateLeadPage extends Annotations {
	
	public UpdateLeadPage() {
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(how=How.ID,using="updateLeadForm_companyName") WebElement eleCmpName;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleUpdate;
		
	public UpdateLeadPage enterCompanyName(String data) {
		clearAndType(eleCmpName, data);
		return this;
	}
	
	public ViewLeadPage clickUpdate() {
		click(eleUpdate);
		return new ViewLeadPage();
	}
	
	
}
