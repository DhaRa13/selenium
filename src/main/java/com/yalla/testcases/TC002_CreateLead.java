package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Creating a Lead";
		author = "Dharani";
		category = "Smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
		
	public void CreateLead(String uName, String pwd,String cmpName, String fstName, String lstName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cmpName)
		.enterFirstName(fstName)
		.enterLastName(lstName)
		.clickCreateLead()
		.verifyLead(fstName);
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}






