package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_MergeLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_MergeLead";
		testcaseDec = "Merging leads details";
		author = "Dharani";
		category = "Smoke";
		excelFileName = "TC004";
	} 

	@Test(dataProvider="fetchData") 
		
	public void MergeLead(String uName, String pwd, String fstName,String cmpName,String cmpName1) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLead()
		.clickFrom()
		.typeFirstName(fstName)
		.typeCompanyName(cmpName)
		.clickFindLead()
		.clickFirstResultingLead()
		.clickTo()
		.typeFirstName(fstName)
		.typeCompanyName(cmpName1)
		.clickFindLead()
		.clickFirstResultingLead()
		.clickMerge();
		
		
	}
	
}






