package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_FindLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_FindLead";
		testcaseDec = "Find & Editing the lead details";
		author = "Dharani";
		category = "Smoke";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
		
	public void FindLead(String uName, String pwd, String fstName, String lstName, String cmpName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.enterFirstName(fstName)
		.enterLastName(lstName)
		.clickFindLead()
		.clickFirstResultingName()
		.clickEdit()
		.enterCompanyName(cmpName)
		.clickUpdate()
		.verifyCompanyName(cmpName);
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}






