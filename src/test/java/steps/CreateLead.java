package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	
	ChromeDriver driver;
	
	@Given("Open the Browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		
	}

	@Given("Maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@Given("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@Given("Enter the username as (.*)")
	public void enterusername(String userName) {
		driver.findElementById("username").sendKeys(userName);
	}

	@Given("Enter the password as (.*)")
	public void enterThePassword(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}

	@When("Click on login button")
	public void clickOnLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@But("Login Failed")
	public void LoginFailed() {
		System.out.println("Login Failed");
	}

	@Then("Click on crmsfa")
	public void clickOnCrmsfa() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Then("Click on Leads")
	public void clickOnLeads() {
		driver.findElementByLinkText("Leads").click();
	}

	@Then("Click on Create Lead")
	public void clickOnCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Then("Enter the CompanyName (.*)")
	public void enterTheCompanyName(String cmpName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cmpName);
	}

	@Then("Enter the FirstName (.*)")
	public void enterTheFirstName(String firstName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);
	}

	@Then("Enter the LastName (.*)")
	public void enterTheLastName(String lastName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);
	}

	/*@Then("Enter the Company Name as Capgemini")
	public void enterTheCompanyName() {
		driver.findElementById("createLeadForm_companyName").sendKeys("Capgemini");
	}

	@Then("Enter the First Name as Dharani")
	public void enterTheFirstName() {
		driver.findElementById("createLeadForm_firstName").sendKeys("Dharani");
	}

	@Then("Enter the Second Name as Mohan")
	public void enterTheSecondName() {
		driver.findElementById("createLeadForm_lastName").sendKeys("Mohan");
	}*/
	
	@When("Click on Create Lead button")
	public void clickOnCreateLeadButton() {
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Successfully created the lead")
	public void successfullyCreatedTheLead() {
	   System.out.println("Created the lead successfully");
	}

}
