Feature: Create Lead TestCase
Background: 
Given Open the Browser
And Maximize the Browser
And Load the URL

Scenario Outline: Create New Lead after successfull login
And Enter the username as DemoSalesManager
And Enter the password as crmsfa
When Click on login button
Then Click on crmsfa
Then Click on Leads
Then  Click on Create Lead
And Enter the CompanyName <CompanyName>
And Enter the FirstName Dharani
And Enter the LastName <LastName>
When Click on Create Lead button
Then Successfully created the lead
Examples:
|CompanyName|LastName|
|Capgemini|Mohan|
|Wipro|M|

Scenario: Negative login
And Enter the username as Democsr
And Enter the password as crmsfa1
When Click on login button
But Login Failed

